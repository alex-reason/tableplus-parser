import { Grammar, Parser } from "nearley";

import uncompiledGrammer from "./grammar";
import { isListT, isMapT, isStringT } from "./termTypes";

// A hack to get typed access to some of the functions in `WorkerGlobalScope`.
// The type definitions for `WorkerGlobalScope` exist in lib.webworker.d.ts, but
// adding 'webworker' to our tsconfig libs would make those globals available in
// all files.
declare const self: Worker;

const grammar = Grammar.fromCompiled(uncompiledGrammer);

const parse = (value: string) => {
  const parser = new Parser(grammar);
  parser.feed(value);
  const result = parser.results[0];

  if (isMapT(result)) {
    result.entries = result.entries.map(({ key, value }) => {
      if (isStringT(value)) {
        try {
          const nestedResult = parse(value.value);
          return { key, value: nestedResult };
        } catch {}
      }

      return { key, value };
    });
  } else if (isListT(result)) {
    result.items = result.items.map((value) => {
      if (isStringT(value)) {
        try {
          const nestedResult = parse(value.value);
          return nestedResult;
        } catch {}
      }

      return value;
    });
  }

  return result;
};

self.onmessage = ({ data }) => {
  try {
    self.postMessage({ isOk: true, value: parse(data) });
  } catch (error) {
    self.postMessage({ isOk: false, error });
  }
};
