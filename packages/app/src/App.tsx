import React, { ReactNode, useState, useEffect } from "react";

// eslint-disable-next-line import/no-webpack-loader-syntax
import Worker from "worker-loader!./parser.worker";
import { isListT, isMapT, isStringT, isTerm, Term } from "./termTypes";
import "./App.css";

const parse = async (value: string) => {
  return new Promise((resolve, reject) => {
    const worker = new Worker();
    worker.onmessage = ({ data }: any) => {
      worker.terminate();
      data.isOk ? resolve(data.value) : reject(data.error);
    };
    worker.postMessage(value);
  });
};

const initialInput =
  '{"regional_overrides":"{\\"{\\\\\\"region\\\\\\":\\\\\\"LSE\\\\\\",\\\\\\"value\\\\\\":\\\\\\"202112\\\\\\",\\\\\\"country_overrides\\\\\\":null}\\"}","value":null}';

function App() {
  const [input, setInput] = useState(initialInput);
  const [result, setResult] = useState<
    { isOk: true; value: any } | { isOk: false; error: any }
  >();

  useEffect(() => {
    parse(input)
      .then((value) => setResult({ isOk: true, value }))
      .catch((error) => setResult({ isOk: false, error }));
  }, [input]);

  let display: ReactNode;
  if (!result) {
    display = null;
  } else if (result.isOk) {
    if (isTerm(result.value)) {
      display = <TermView result={result.value} />;
    } else {
      display = JSON.stringify(result.value);
    }
  } else {
    display = <pre>{result.error?.toString()}</pre>;
  }

  return (
    <div className="App">
      <textarea
        rows={8}
        value={input}
        onChange={({ target }) => setInput(target.value)}
      />
      <div className="AppContent">{display}</div>
    </div>
  );
}

const TermView = ({ result }: { result: Term }) => {
  if (isListT(result)) {
    return (
      <div className="list">
        {result.items.length > 0 ? (
          <ul>
            {result.items.map((value, i) => (
              <li key={i}>
                <TermView result={value} />
              </li>
            ))}
          </ul>
        ) : (
          <em>Empty collection</em>
        )}
      </div>
    );
  }

  if (isMapT(result)) {
    return (
      <div className="map">
        {result.entries.length > 0 ? (
          <table>
            <tbody>
              {result.entries.map(({ key, value }, i) => (
                <tr key={i}>
                  <th>
                    <TermView result={key} />
                  </th>
                  <td>
                    <TermView result={value} />
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        ) : (
          <em>Empty collection</em>
        )}
      </div>
    );
  }

  if (isStringT(result)) {
    if (/^[a-z]+:\/\/./i.test(result.value)) {
      return (
        <div className="string string--link">
          <a href={result.value}>{result.value}</a>
        </div>
      );
    }

    return <div className="string">{result.value}</div>;
  }

  return <pre className="literal">{result.value}</pre>;
};

export default App;
