// Generated automatically by nearley, version 2.19.6
// http://github.com/Hardmath123/nearley
// Bypasses TS6133. Allow declared but unused functions.
// @ts-ignore
function id(d: any[]): any { return d[0]; }

interface NearleyToken {  value: any;
  [key: string]: any;
};

interface NearleyLexer {
  reset: (chunk: string, info: any) => void;
  next: () => NearleyToken | undefined;
  save: () => any;
  formatError: (token: NearleyToken) => string;
  has: (tokenType: string) => boolean;
};

interface NearleyRule {
  name: string;
  symbols: NearleySymbol[];
  postprocess?: (d: any[], loc?: number, reject?: {}) => any;
};

type NearleySymbol = string | { literal: any } | { test: (token: any) => boolean };

interface Grammar {
  Lexer: NearleyLexer | undefined;
  ParserRules: NearleyRule[];
  ParserStart: string;
};

const grammar: Grammar = {
  Lexer: undefined,
  ParserRules: [
    {"name": "MAIN", "symbols": ["_", "COLLECTION", "_"], "postprocess": d => d[1]},
    {"name": "COLLECTION", "symbols": ["LIST"], "postprocess": id},
    {"name": "COLLECTION", "symbols": ["MAP"], "postprocess": id},
    {"name": "LIST", "symbols": [{"literal":"{"}, "_", "LIST_ITEMS", "_", {"literal":"}"}], "postprocess": d => ({ type: "list", items: d[2] })},
    {"name": "LIST_ITEMS", "symbols": [], "postprocess": _ => []},
    {"name": "LIST_ITEMS", "symbols": ["LIST_NON_EMPTY_ITEMS"], "postprocess": id},
    {"name": "LIST_NON_EMPTY_ITEMS", "symbols": ["VALUE"]},
    {"name": "LIST_NON_EMPTY_ITEMS", "symbols": ["VALUE", "_", {"literal":","}, "_", "LIST_NON_EMPTY_ITEMS"], "postprocess": d => [d[0], ...d[4]]},
    {"name": "MAP", "symbols": [{"literal":"{"}, "_", "MAP_ENTRIES", "_", {"literal":"}"}], "postprocess": d => ({ type: "map", entries: d[2] })},
    {"name": "MAP_ENTRIES", "symbols": [], "postprocess": _ => []},
    {"name": "MAP_ENTRIES", "symbols": ["MAP_NON_EMPTY_ENTRIES"], "postprocess": id},
    {"name": "MAP_NON_EMPTY_ENTRIES", "symbols": ["MAP_ENTRY"]},
    {"name": "MAP_NON_EMPTY_ENTRIES", "symbols": ["MAP_ENTRY", "_", {"literal":","}, "_", "MAP_NON_EMPTY_ENTRIES"], "postprocess": d => [d[0], ...d[4]]},
    {"name": "MAP_ENTRY", "symbols": ["VALUE", "_", {"literal":":"}, "_", "VALUE"], "postprocess": d => ({ key: d[0], value: d[4] })},
    {"name": "VALUE", "symbols": ["STRING"], "postprocess": id},
    {"name": "VALUE$ebnf$1", "symbols": []},
    {"name": "VALUE$ebnf$1", "symbols": ["VALUE$ebnf$1", /[^":,{}]/], "postprocess": (d) => d[0].concat([d[1]])},
    {"name": "VALUE", "symbols": [/[^":,{} ]/, "VALUE$ebnf$1"], "postprocess": d => ({ type: "literal", value: d[0] + d[1].join('') })},
    {"name": "STRING$ebnf$1", "symbols": []},
    {"name": "STRING$ebnf$1", "symbols": ["STRING$ebnf$1", "CHAR"], "postprocess": (d) => d[0].concat([d[1]])},
    {"name": "STRING", "symbols": [{"literal":"\""}, "STRING$ebnf$1", {"literal":"\""}], "postprocess": d => ({ type: "string", value: d[1].join('') })},
    {"name": "CHAR", "symbols": [{"literal":"\\"}, /./], "postprocess": d => d[1]},
    {"name": "CHAR", "symbols": [/[^\\"]/], "postprocess": id},
    {"name": "_$ebnf$1", "symbols": []},
    {"name": "_$ebnf$1", "symbols": ["_$ebnf$1", {"literal":" "}], "postprocess": (d) => d[0].concat([d[1]])},
    {"name": "_", "symbols": ["_$ebnf$1"], "postprocess": _ => null}
  ],
  ParserStart: "MAIN",
};

export default grammar;
