declare module "worker-loader!*" {
  // Change `Worker` if you specified a different `workerType` option.
  class WebpackWorker extends Worker {
    constructor();
  }

  // Uncomment this if you set the `esModule` option to `false`
  // export = WebpackWorker;
  export default WebpackWorker;
}
