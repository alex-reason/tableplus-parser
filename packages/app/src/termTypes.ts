export type StringT = {
  type: "string";
  value: string;
};
export const isStringT = (value: any): value is StringT =>
  value?.type === "string";

export type LiteralT = {
  type: "literal";
  value: string;
};

export const isLiteralT = (value: any): value is LiteralT =>
  value?.type === "literal";

export type ValueT = StringT | LiteralT;
export const isValueT = (value: any): value is ValueT =>
  isStringT(value) || isLiteralT(value);

export type ListT = {
  type: "list";
  items: ValueT[];
};
export const isListT = (value: any): value is ListT => value?.type === "list";

export type MapT = {
  type: "map";
  entries: { key: ValueT; value: ValueT }[];
};
export const isMapT = (value: any): value is MapT => value?.type === "map";

export type Term = ValueT | ListT | MapT;
export const isTerm = (value: any): value is Term =>
  isValueT(value) || isListT(value) || isMapT(value);
