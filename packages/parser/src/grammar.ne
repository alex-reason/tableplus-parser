@preprocessor typescript

MAIN -> _ COLLECTION _                        {% d => d[1] %}

COLLECTION
  -> LIST                                     {% id %}
  |  MAP                                      {% id %}

# Covers output for lists and sets
LIST -> "{" _ LIST_ITEMS _ "}"                {% d => ({ type: "list", items: d[2] }) %}

LIST_ITEMS
  -> null                                     {% _ => [] %}
  |  LIST_NON_EMPTY_ITEMS                     {% id %}

LIST_NON_EMPTY_ITEMS
  -> VALUE
  |  VALUE _ "," _ LIST_NON_EMPTY_ITEMS       {% d => [d[0], ...d[4]] %}

# Covers output for maps and UDTs
MAP -> "{" _ MAP_ENTRIES _ "}"                {% d => ({ type: "map", entries: d[2] }) %}

MAP_ENTRIES
  -> null                                     {% _ => [] %}
  |  MAP_NON_EMPTY_ENTRIES                    {% id %}

MAP_NON_EMPTY_ENTRIES
  -> MAP_ENTRY
  |  MAP_ENTRY _ "," _ MAP_NON_EMPTY_ENTRIES  {% d => [d[0], ...d[4]] %}

MAP_ENTRY -> VALUE _ ":" _ VALUE              {% d => ({ key: d[0], value: d[4] }) %}

VALUE
  -> STRING                                   {% id %}
  |  [^":,{} ] [^":,{}]:*                     {% d => ({ type: "literal", value: d[0] + d[1].join('') }) %}

STRING -> "\"" CHAR:* "\""                    {% d => ({ type: "string", value: d[1].join('') }) %}

CHAR
  -> "\\" .                                   {% d => d[1] %}
  |  [^\\"]                                   {% id %}

_ -> " ":*                                    {% _ => null %}
